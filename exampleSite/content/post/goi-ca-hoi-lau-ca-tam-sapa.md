---
title: Gỏi cá hồi & Lẩu cá tầm Sapa
date: 2022-03-01T00:00:00+07:00
image: images/blog/blog-img-1.jpg
description: this is meta description

---
**Gỏi cá hồi**

Thịt cá hồi ăn gỏi được lát mỏng bày ra đĩa thành hình tròn hoặc xếp thành hình bông hoa cùng với rau sống và các gia vị khác nhìn rất bắt mắt.

Cá hồi sẽ được ăn kèm cùng những thức vị chua cay như khế chua, ớt, dứa xanh, lá sấu… Khi ăn, quấn gỏi cá cùng rau sống và thức đi kèm sau đó chấm gỏi cá hồi vào mùi tạt để gỏi được ngon nhất.

**Lẩu cá tầm**

Nguyên liệu chính để tạo nên món lẩu này là đầu cá hồi, cá tầm hầm cùng với các loại củ quả để làm nước lẩu, bên cạnh đó không thể thiếu hành, thì là để làm giảm bớt mùi tanh của cá và tăng thêm hương vị cho món lẩu. Thịt cá tầm, cá hồi được thái mỏng để nhúng lẩu cùng với các loại rau đặc sản Sa Pa như cải mèo, su su hoặc rau muống, cải thảo...

Giữa không khí mát lạnh của buổi tối ở Sa Pa, ngồi bên nồi lẩu cá tầm cá hồi nóng hổi, xì xụp nhúng cá nhúng rau thì còn gì tuyệt vời bằng. Cá ở đây tươi hơn hẳn do được làm luôn sau khi đánh bắt, bởi thế mà từng thớ thịt thơm ngọt thấy rõ.

Rau nhúng lẩu lại là các loại rau đặc sản núi rừng Sa Pa nên khá lạ miệng đối với các du khách. Ăn kèm với lẩu cá tầm cá hồi còn có bún rối, rất hợp với nước lẩu đã được ninh ngọt lịm.